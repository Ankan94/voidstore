using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using memes.Controllers;
using memes.Models;

namespace memes.Repositories
{
    public class InMemoryNoteRepository : INoteRepository
    {
        private readonly Dictionary<int, Note> _notes = new Dictionary<int, Note>();
        private int _count = 1;

        public void Delete(int id)
        {
            _notes.Remove(id);
        }

        public Note Find(int id)
        {
            if (!_notes.ContainsKey(id)) {
                return null;
            }
            return _notes[id];
        }

        public IEnumerable<Note> FindAll()
        {
            return _notes.Values;
        }

        public void Save(Note note)
        {
            note.Id = _count;
            _notes.Add(note.Id, note);
            
            _count++;
        }

        public void Update(Note note)
        {
            _notes[note.Id] = note;
        }
    }
}