﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using memes.Controllers;
using System.Data.SqlClient;
using Dapper;
using memes.Models;

namespace memes.Repositories
{
    public class NoteRepository : INoteRepository
    {
        private readonly SqlConnection _connection;

        public NoteRepository(SqlConnection connection)
        {
            _connection = connection;
        }

        public void Delete(int id)
        {
            var count = _connection.Execute(@"DELETE Note WHERE Id = @id", new { id });
        }

        public Note Find(int id)
        {
            return _connection.Query<Note>(@"SELECT * FROM Note
                                           WHERE Id = @id", new { id }).SingleOrDefault();
        }

        public IEnumerable<Note> FindAll()
        {
            return _connection.Query<Note>(@"SELECT * FROM Note");
        }

        public void Save(Note note)
        {
            var count = _connection.Execute(@"INSERT Note (Summary, Text, CreatedDate)
                                            VALUES (@Summary, @Text, @CreatedDate)",
                                            note);
        }

        public void Update(Note note)
        {
            var count = _connection.Execute(@"UPDATE Note
                                            SET Summary = @Summary,
                                            Text = @Text,
                                            CreatedDate = @CreatedDate
                                            WHERE Id = @Id",
                                            note);
        }
    }
}
