using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using memes.Repositories;
using Microsoft.AspNetCore.Mvc;
using memes.Models;

namespace memes.Controllers
{
    [Route("api/[controller]")]
    public class NotesController : Controller
    {
        private readonly INoteRepository _repository;

        public NotesController(INoteRepository repository) {
            _repository = repository;
        }

        // GET api/values
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_repository.FindAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var note = _repository.Find(id);

            if (note == null) {
                return NotFound();
            }

            return Ok(note);
        }

        // POST api/values
        [HttpPost]
        public ActionResult Post([FromBody]Note note)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            note.CreatedDate = DateTime.Now;
            _repository.Save(note);

            return Ok(note);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
