﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace memes.Models
{
    public class Note
    {
        public int Id { get; set; }
        public string Summary { get; set; }
        public string Text { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
